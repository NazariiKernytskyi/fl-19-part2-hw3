function firstLoad() {
  const mainBlock = document.querySelector('.user-about');

  fetch('http://localhost:8080/api/users/me', {
    method: 'GET',
    headers: {
      'content-type': 'application/json; charset=utf-8',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    },

  })
    .then((response) => {
      response.json().then((data) => {
        console.log(data);
        mainBlock.innerHTML = `
            My id : ${data.user._id} <br>
            My email: ${data.user.email} <br>
            Created Data: ${data.user.createdDate}`;
      });
    });

  console.log('DOM готов!');
}

document.addEventListener('DOMContentLoaded', firstLoad);

const logOut = document.querySelector('.logout');
logOut.addEventListener('click', () => {
  localStorage.removeItem('token');
  const url = 'http://127.0.0.1:5500/app/index.html';
  window.location.assign(url);
});
