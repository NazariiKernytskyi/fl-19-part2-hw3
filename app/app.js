const loginBtn = document.getElementById('login-btn');
const registerBtn = document.getElementById('register-btn');
const changeBtn = document.getElementById('change-btn');
const changeBack = document.getElementById('change-back-btn');

registerBtn.addEventListener('click', () => {
  const email = document.querySelector('.register-username').value;
  const password = document.querySelector('.register-password').value;
  const radioBtn = document.getElementsByName('role');

  for (const radio of radioBtn) {
    if (radio.checked) {
      const role = radio.value.toUpperCase();
      fetch('http://localhost:8080/api/auth/register', {
        method: 'POST',
        headers: { 'content-type': 'application/json; charset=utf-8' },
        body: JSON.stringify({
          email,
          password,
          role,
        }),
      })
        .then((response) => {
          if (response.ok) {
            response.json().then(() => {
              window.location.reload();
            });
          } else {
            response.json().then(() => {
              console.log('Something went wrong');
            });
          }
        });
    }
  }
});

loginBtn.addEventListener('click', () => {
  const email = document.querySelector('.login-username').value;
  const password = document.querySelector('.login-password').value;

  fetch('http://localhost:8080/api/auth/login', {
    method: 'POST',
    headers: { 'content-type': 'application/json; charset=utf-8' },
    body: JSON.stringify({
      email,
      password,
    }),
  })
    .then((response) => {
      if (response.ok) {
        response.json().then((data) => {
          console.log(data);
          localStorage.setItem('token', data.jwt_token);
          fetch('http://localhost:8080/api/users/me', {
            method: 'GET',
            headers: {
              'content-type': 'application/json; charset=utf-8',
              authorization: `Bearer ${localStorage.getItem('token')}`,
            },

          })
            .then((r) => {
              r.json().then((d) => {
                if (d.user.role === 'DRIVER') {
                  const url = 'http://127.0.0.1:5500/app/login-driver.html';
                  window.location.assign(url);
                } else {
                  const url = 'http://127.0.0.1:5500/app/login-shipper.html';
                  window.location.assign(url);
                }
              });
            });
        });
      } else {
        console.log('error');
      }
    });
});

const loginPage = document.querySelector('.login-page');
const registerPage = document.querySelector('.register-page');
changeBtn.addEventListener('click', () => {
  loginPage.classList.add('display-none');
  registerPage.classList.remove('display-none');
});

changeBack.addEventListener('click', () => {
  loginPage.classList.remove('display-none');
  registerPage.classList.add('display-none');
});
