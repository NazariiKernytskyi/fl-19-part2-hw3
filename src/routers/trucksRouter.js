const express = require('express');

const router = express.Router();
const {
  addUserTruck, getUserTrucks, getUserTruckById, updateUserTruckById,
  assignUserTruckById, deleteUserTruckById,
} = require('../services/trucksService');
const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, addUserTruck);

router.get('/', authMiddleware, getUserTrucks);

router.get('/:id', authMiddleware, getUserTruckById);

router.put('/:id', authMiddleware, updateUserTruckById);

router.post('/:id/assign', authMiddleware, assignUserTruckById);

router.delete('/:id', authMiddleware, deleteUserTruckById);

module.exports = {
  trucksRouter: router,
};
