const express = require('express');

const router = express.Router();

const {
  addUserLoad, getUserLoads, getUserActiveLoad, iterateLoadState, getUserLoadById,
  updateUserLoadById, deleteUserLoadById, postUserLoadById, getUserLoadShippingDetailsById,
} = require('../services/loadsService');

const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, addUserLoad);

router.get('/', authMiddleware, getUserLoads);

router.get('/active', authMiddleware, getUserActiveLoad);

router.patch('/active/state', authMiddleware, iterateLoadState);

router.get('/:id', authMiddleware, getUserLoadById);

router.put('/:id', authMiddleware, updateUserLoadById);

router.delete('/:id', authMiddleware, deleteUserLoadById);

router.post('/:id/post', authMiddleware, postUserLoadById);

router.get('/:id/shipping_info', authMiddleware, getUserLoadShippingDetailsById);

module.exports = {
  loadsRouter: router,
};
