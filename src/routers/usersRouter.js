const express = require('express');

const router = express.Router();
const { getProfileInfo, deleteUser, changeProfilePassword } = require('../services/usersService');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, getProfileInfo);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me/password', authMiddleware, changeProfilePassword);

module.exports = {
  usersRouter: router,
};
