const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
    require: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

const Truck = mongoose.model('trucks', truckSchema);

module.exports = {
  Truck,
};
