const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
  },
  status: {
    type: String,
    default: 'NEW',
    required: false,
  },
  state: {
    type: String,    
    required: false,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: { type: Number, required: false },
    length: { type: Number, required: false },
    height: { type: Number, required: false },
  },
  logs: {
    type: Array,
    required: false,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

const Load = mongoose.model('loads', loadSchema);

module.exports = {
  Load,
};
