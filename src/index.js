const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();

const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://NazariiKernytskyi:NazariiKernytskyi@cluster0.tkcjcia.mongodb.net/HW-3?retryWrites=true&w=majority');

const { authRouter } = require('./routers/authRouter');
const { usersRouter } = require('./routers/usersRouter');
const { trucksRouter } = require('./routers/trucksRouter');
const { loadsRouter } = require('./routers/loadsRouter');

app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

const start = async () => {
  try {
    app.listen(8080);
    console.log('port is working');
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
app.use(errorHandler);
