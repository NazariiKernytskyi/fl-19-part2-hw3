const { Truck } = require('../models/Truck');

function addUserTruck(req, res) {
  const { type } = req.body;
  const { userId } = req.user;
  const truck = new Truck({
    created_by: userId,
    type,
  });
  truck.save().then(
    res.status(200).json({ message: 'Truck created successfully' }),
  );
}

const getUserTrucks = (req, res) => Truck.find({ created_by: req.user.userId })
  .then((result) => {
    if (result.length > 0) {
      res.status(200).json({ trucks: result });
    } else {
    res.status(200).json({ message: 'No trucks added yet' });
    }
  }
);


const getUserTruckById = (req, res) => Truck.findById(req.params.id)
.then((result) => {
  res.status(200).json({ truck: result });
});


const updateUserTruckById = async (req, res) => {
  const { type } = req.body;
  await Truck.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, {
    $set: { type },
  });
  return res.status(200).json({ message: 'Truck details changed successfully' });
};

const deleteUserTruckById = (req, res) => Truck.findByIdAndDelete(req.params.id)
  .then(() => {
    res.status(200).json({ message: 'Truck deleted successfully' });
  });

const assignUserTruckById = async (req, res) => {
  await Truck.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { assigned_to: req.user.userId } },
  );
  res.status(200).json({ message: 'Truck assigned successfully' });
};

// const assignUserTruckById = (req, res) => {
//   const { assigned_to } = req.body;
//   return Truck.findByIdAndUpdate(
//     { _id: req.params.id, userId: req.user.userId },
//     { $set: { assigned_to: assigned_to} },
//   ).then(() => {
//     res.status(200).json({ message: 'Truck assigned successfully' });
//   });
// }

module.exports = {
  addUserTruck,
  getUserTrucks,
  getUserTruckById,
  updateUserTruckById,
  assignUserTruckById,
  deleteUserTruckById,
};
