const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const nodemailer = require('nodemailer');
const { User, userJoiSchema } = require('../models/Users');


const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body;

  try {
    await userJoiSchema.validateAsync({ email, password });
    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });

    return user.save()
      .then(() => res.json({ message: 'Profile created successfully' }))
      .catch((err) => {
        res.status(400).json({ message: err.message });
      });
    
  } catch (error) {
    return res.status(401).json({ message: 'You need to create stronger password' });    
  }
  
};

const loginUser = async (req, res) => {
  console.log(req.body);
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.json({ jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;
  const temporaryPassword = 'test12345';
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    res.status(400).json({ message: 'Wrong email address' });
  } else {
    User.findOneAndUpdate(
      { email: req.body.email },
      { $set: { password: await bcrypt.hash(temporaryPassword, 10) } },
    )
      .then(() => {
        const mailTransporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'kernytskyi.nazar@gmail.com',
            pass: 'ecexjfzuyuxemhsh',
          },
        });

        const mailDetails = {
          from: 'kernytskyi.nazar@gmail.com',
          to: `${email}`,
          subject: 'Reset password task from HW_3',
          text: `Your new temporary is - ${temporaryPassword}`,
        };

        mailTransporter.sendMail(mailDetails, (err) => {
          if (err) {
            console.log(err);
          } else {
            res.status(200).json({ message: 'New password sent to your email address' });
          }
        });
      });
  }
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
};
