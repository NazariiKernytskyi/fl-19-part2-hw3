const { Load } = require('../models/Load');
const { Truck } = require('../models/Truck');

function addUserLoad(req, res) {
  const { userId } = req.user;
  const { name } = req.body;
  const { payload } = req.body;
  const { pickup_address } = req.body;
  const { delivery_address } = req.body;
  const load = new Load({
    created_by: userId,
    name,
    payload,
    pickup_address,
    delivery_address,

  });
  load.save().then(() => {
    load.updateOne({
      $push: {
        logs: {
          message: 'Load created successfully',
          time: new Date().toISOString(),
        },
      },
    })
      .then(() => {
        res.status(200).json({ message: 'Load created successfully' });
      });
  });
}

const getUserLoads = (req, res) => Load.find({ userId: req.user.userId }, '-__v').then((result) => {
  res.status(200).json({ loads: result });
});

const getUserActiveLoad = (req, res) => Load.findOne({ assigned_to: req.user.userId })
  .then((result) => {
    res.status(200).json({ load: result });
  });

const iterateLoadState = (req, res) => {
  const loadTransitions = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
  ];

  Load.findOne({ assigned_to: req.user.userId }).then((result) => {
    if (result.status === 'ASSIGNED') {
      const currentState = loadTransitions.indexOf(result.state);
      const newState = loadTransitions[currentState + 1];

      Load.findOneAndUpdate({ assigned_to: req.user.userId }, {
        $set: { state: newState },
        $push: {
          logs: {
            message: `Load state changed to ${newState}`,
            time: new Date().toISOString(),
          },
        },
      })
        .then(() => {
          if (newState === loadTransitions[3]) {
            Load.findOneAndUpdate({ assigned_to: req.user.userId }, {
              $set: { status: 'SHIPPED' },
              $push: {
                logs: {
                  message: 'Load status changed to SHIPPED',
                  time: new Date().toISOString(),
                },
              },
            })
              .then(() => {
                Truck.findOneAndUpdate({ assigned_to: req.user.userId }, { $set: { status: 'IS' } })
                  .then(() => {
                    res.status(200).json({ message: `Load state changed to ${newState}` });
                  });
              });
          } else {
            res.status(200).json({ message: `Load state changed to ${newState}` });
          }
        });
    } else {
      res.status(400).json({ message: 'There is no such load available' });
    }
  });
};

const getUserLoadById = async (req, res) => {
  const result = await Load.findById(req.params.id);
  res.status(200).json({ load: result });
};

const updateUserLoadById = async (req, res) => {
  const {
    name, payload, dimensions, pickup_address, delivery_address,
  } = req.body;
  await Load.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, {
    $set: {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    },
  });
  res.status(200).json({ message: 'Load details changed successfully' });
};

const deleteUserLoadById = (req, res) => Load.findByIdAndDelete(req.params.id)
  .then(() => { res.status(200).json({ message: 'Load deleted successfully' }); });

const postUserLoadById = (req, res) => {
  Load.findByIdAndUpdate(req.params.id, {
    $set: { status: 'POSTED' },
    $push: {
      logs: {
        message: 'Load status changed to POSTED',
        time: new Date().toISOString(),
      },
    },
  })
    .then(() => {
      Truck.find({ status: 'IS' }).then((result) => {
        if (result.length > 0) {
          const driverWithTruck = result.filter((element) => element.assigned_to);

          if (driverWithTruck.length > 0) {
            Truck.findByIdAndUpdate(driverWithTruck[0]._id, { $set: { status: 'OL' } })
              .then(() => {
                Load.findByIdAndUpdate(req.params.id, {
                  $set: {
                    status: 'ASSIGNED',
                    state: 'En route to Pick Up',
                    assigned_to: driverWithTruck[0].assigned_to,
                  },
                  $push: {
                    logs: {
                      message: `Load ASSIGNED to DRIVER with id ${driverWithTruck[0].assigned_to}`,
                      time: new Date().toISOString(),
                    },
                  },
                })

                  .then(() => res.status(200).json({ message: 'Load posted successfully', driver_found: true }));
              });
          } else {
            Load.findByIdAndUpdate(req.params.id, {
              $set: { status: 'NEW' },
              $push: {
                logs: {
                  message: 'DRIVER not found. Status changed to NEW',
                  time: new Date().toISOString(),
                },
              },
            })
              .then(() => res.status(400).json({ message: 'Load posted successfully', driver_found: false }));
          }
        } else {
          Load.findByIdAndUpdate(req.params.id, {
            $set: { status: 'NEW' },
            $push: {
              logs: {
                message: 'TRUCK not found. Status changed to NEW',
                time: new Date().toISOString(),
              },
            },
          })
            .then(() => res.status(400).json({ message: 'Load posted successfully', driver_found: false }));
        }
      });
    });
};

const getUserLoadShippingDetailsById = async (req, res) => {
  const loadInfo = await Load.findById(req.params.id);
  Load.findById(req.params.id).then((r) => {
    Truck.findOne({ assigned_to: r.assigned_to }).then((result) => {
      res.status(200).json({
        load: loadInfo,
        truck: result,
      });
    });
  });
};

module.exports = {
  addUserLoad,
  getUserLoads,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingDetailsById,
};
